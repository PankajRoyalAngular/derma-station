export const environment = {
  production: true,
  baseUrl: ' https://dermaquestapi.azurewebsites.net/api/',
  baseImageUrl: 'https://dermaquestapi.azurewebsites.net/',
};
