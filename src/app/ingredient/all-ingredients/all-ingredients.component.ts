import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { LanguageService, MenuService } from 'src/app/shared/services';

@Component({
  selector: 'app-all-ingredients',
  templateUrl: './all-ingredients.component.html',
  styleUrls: ['./all-ingredients.component.css'],
})
export class AllIngredientsComponent implements OnInit {
  allingredientsList: any;
  currentLang: any;
  constructor(
    private menuService: MenuService,
    private spinner: NgxSpinnerService,
    private router: Router,
    private languageService: LanguageService
  ) {
    this.languageService.currentLangauge.subscribe((res: any) => {
      if (res == null || res == 'en') {
        this.currentLang = 'en';
      } else {
        this.currentLang = res;
      }
    });
  }

  ngOnInit(): void {
    this.getAllIngredients();
  }

  getAllIngredients() {
    this.spinner.show();
    this.menuService.getAllIngredients().subscribe((res: any) => {
      const sorted = res.data.sort((a, b) =>
        a.ingredientName > b.ingredientName ? 1 : -1
      );
      const grouped = sorted.reduce((groups, contact) => {
        const letter = contact.ingredientName.charAt(0);
        groups[letter] = groups[letter] || [];
        groups[letter].push(contact);
        return groups;
      }, {});

      const result = Object.keys(grouped).map((key) => ({
        key,
        contacts: grouped[key],
      }));
      this.allingredientsList = result;
      this.spinner.hide();
    });
  }

  gotoDiv(content: any) {
    document.getElementById(content).scrollIntoView({ behavior: 'smooth' });
  }

  getIngredenitsDetails(data: any) {
    this.menuService.sharedData(data);
    localStorage.setItem('ingedientsData', JSON.stringify(data));
    this.router.navigate([
      'ingredient/ingredients-product-list',
      btoa(data.ingredientId),
    ]);
  }
}
