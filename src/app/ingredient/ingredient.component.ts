import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ingredient',
  templateUrl: './ingredient.component.html',
  styleUrls: ['./ingredient.component.css'],
})
export class IngredientComponent implements OnInit {
  constructor(private router: Router) {
    if (this.router.url === '/ingredient') {
      this.router.navigateByUrl('ingredient/all-ingredients');
    }
  }

  ngOnInit(): void {}
}
