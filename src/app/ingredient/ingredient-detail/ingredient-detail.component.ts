import { Component, OnInit, TemplateRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { ProductDetailService, MenuService } from 'src/app/shared/services';
import { AddToBagService } from 'src/app/shared/services/add-to-bag.service';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-ingredient-detail',
  templateUrl: './ingredient-detail.component.html',
  styleUrls: ['./ingredient-detail.component.css'],
})
export class IngredientDetailComponent implements OnInit {
  ingredientProductDetails: any;
  cartValue: any = 0;
  baseImageUrl = environment.baseImageUrl;
  myThumbnail: string;
  modalRef: BsModalRef;
  constructor(
    private route: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private productDetailService: ProductDetailService,
    private menuService: MenuService,
    private modalService: BsModalService,
    private addtocartService: AddToBagService
  ) {
    this.route.params.subscribe((res: any) => {
      {
        this.spinner.show();
        this.productDetailService
          .getProductInfoById(atob(res['id']))
          .subscribe((res: any) => {
            if (res.status) {
              this.spinner.hide();
              this.ingredientProductDetails = res.data;
              console.log(this.ingredientProductDetails);
              this.myThumbnail =
                this.baseImageUrl + this.ingredientProductDetails.imagePaths[0];
            } else {
              this.spinner.hide();
              this.menuService.sharedData(null);
            }
          });
      }
    });
  }

  ngOnInit(): void {}

  openAddToCart(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
    var data = {
      cartId: 0,
      quantity: 1,
      price: 25,
      productId: 14,
    };
    this.addtocartService.addToCart(data).subscribe((res: any) => {
      console.log(res);
    });
  }
}
