import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AllIngredientsComponent } from './all-ingredients/all-ingredients.component';
import { IngredientComponent } from './ingredient.component';
import { IngredientsProductListComponent } from './ingredients-product-list/ingredients-product-list.component';
import { IngredientDetailComponent } from './ingredient-detail/ingredient-detail.component';

const routes: Routes = [
  {
    path: '',
    component: IngredientComponent,
  },
  {
    path: 'all-ingredients',
    component: AllIngredientsComponent,
  },
  {
    path: 'ingredients-product-list',
    component: IngredientsProductListComponent,
  },
  {
    path: 'ingredients-product-list/:id',
    component: IngredientsProductListComponent,
  },
  {
    path: 'ingredients-detail/:id',
    component: IngredientDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IngredientRoutingModule {}
