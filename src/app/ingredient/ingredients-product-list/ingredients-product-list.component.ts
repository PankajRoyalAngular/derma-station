import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { MenuService, LanguageService } from 'src/app/shared/services';
import { ProductService } from '../../shared/services/product.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-ingredients-product-list',
  templateUrl: './ingredients-product-list.component.html',
  styleUrls: ['./ingredients-product-list.component.css'],
})
export class IngredientsProductListComponent implements OnInit {
  ingredientsData: any;
  currentLang: any;
  rate: any;
  baseImageUrl = environment.baseImageUrl;
  subscription: Subscription;
  titleValue: any;
  isShownodata: boolean = false;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private languageService: LanguageService,
    private productService: ProductService,
    private toaster: ToastrService,
    private spinner: NgxSpinnerService
  ) {
    this.languageService.currentLangauge.subscribe((res: any) => {
      if (res == null || res == 'en') {
        this.currentLang = 'en';
      } else {
        this.currentLang = res;
      }
    });
    if (this.router.url == '/ingredient/ingredients-product-list') {
      this.router.navigateByUrl('ingredient/all-ingredients');
    }
  }

  ngOnInit(): void {
    this.spinner.show();
    this.route.params.subscribe((params) => {
      var productData = {
        pageNumber: 1,
        pageSize: 10,
        inputValue: atob(params['id']),
      };
      // Use for future to send id console.log(params['id']);
      this.productService
        .getProductDetailsForIngredient(productData)
        .subscribe((res: any) => {
          if (res.data.dataList.length === 0) {
            this.isShownodata = true;
            this.spinner.hide();
          } else {
            this.isShownodata = false;
            this.ingredientsData = res.data.dataList;
            this.titleValue = res.data.dataList;
            this.rate = res.data.dataList.rating;
            this.spinner.hide();
          }
        });
    });
  }

  productDetails(data: any) {
    setTimeout(function () {
      var elmnt = document.getElementById('backtotop');
      elmnt.scrollIntoView();
    }, 500);
    this.router.navigate([
      '/ingredient/ingredients-detail',
      btoa(data.productId),
    ]);
  }
  ngAfterViewInit() {
    window.scrollTo(0, 0);
  }
}
