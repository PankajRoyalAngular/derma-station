import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IngredientsProductListComponent } from './ingredients-product-list.component';

describe('IngredientsProductListComponent', () => {
  let component: IngredientsProductListComponent;
  let fixture: ComponentFixture<IngredientsProductListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IngredientsProductListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IngredientsProductListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
