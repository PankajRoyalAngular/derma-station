import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IngredientRoutingModule } from './ingredient-routing.module';
import { IngredientComponent } from './ingredient.component';
import { SharedModule } from '../shared/shared.module';
import { IngredientsProductListComponent } from './ingredients-product-list/ingredients-product-list.component';
import { AllIngredientsComponent } from './all-ingredients/all-ingredients.component';
import { IngredientDetailComponent } from './ingredient-detail/ingredient-detail.component';

@NgModule({
  declarations: [
    IngredientComponent,
    AllIngredientsComponent,
    IngredientsProductListComponent,
    IngredientDetailComponent,
  ],
  imports: [CommonModule, IngredientRoutingModule, SharedModule],
})
export class IngredientModule {}
