import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { environment } from 'src/environments/environment';
import { Filters } from '../shared/modals/filter';
import { BlogService } from '../shared/services';
import { LanguageService } from '../shared/services/language.service';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css'],
})
export class BlogComponent implements OnInit {
  filters: Filters = {
    sortOrder: '',
    sortField: '',
    pageNumber: 1,
    pageSize: 10,
    productCategoryId: 1,
    searchQuery: '',
    filterBy: '',
  };
  currentLang: any = 'en';
  page: 1;
  blogsData: any;
  baseImageUrl = environment.baseImageUrl;
  constructor(
    private blogService: BlogService,
    private spinner: NgxSpinnerService,
    private router: Router,
    private languageService: LanguageService
  ) {}

  ngOnInit() {
    this.languageService.currentLangauge.subscribe((res: any) => {
      if (res == null || res == 'en') {
        this.currentLang = 'en';
      } else {
        this.currentLang = res;
      }
    });
    this.getAllBlogs();
  }

  getAllBlogs() {
    this.spinner.show();
    this.blogService.getAllBlogs(this.filters).subscribe((res: any) => {
      this.blogsData = res.data.dataList;
      this.spinner.hide();
    });
  }

  getSkinCareBlogs() {
    this.page = 1;
    this.filters.productCategoryId = 1;
    this.spinner.show();
    this.blogService
      .getBlogListByCategory(this.filters)
      .subscribe((res: any) => {
        this.blogsData = res.data.dataList;
        this.spinner.hide();
      });
  }

  getHairCareBlogs() {
    this.page = 1;
    this.filters.productCategoryId = 2;
    this.spinner.show();
    this.blogService
      .getBlogListByCategory(this.filters)
      .subscribe((res: any) => {
        this.blogsData = res.data.dataList;
        this.spinner.hide();
      });
  }

  goTotop() {
    setTimeout(function () {
      var elmnt = document.getElementById('top');
      elmnt.scrollIntoView({
        behavior: 'smooth',
      });
    }, 0);
  }

  readMore(data: any) {
    setTimeout(function () {
      var elmnt = document.getElementById('backtotop');
      elmnt.scrollIntoView({ behavior: 'smooth' });
    }, 100);
    this.router.navigate(['/blog/blog-details/', data.id]);
  }
}
