import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { environment } from 'src/environments/environment.prod';
import { BlogService } from '../../shared/services/blog.service';
import { LanguageService } from '../../shared/services/language.service';

@Component({
  selector: 'app-blog-detail',
  templateUrl: './blog-detail.component.html',
  styleUrls: ['./blog-detail.component.css'],
})
export class BlogDetailComponent implements OnInit {
  blogDetails: any;
  blogImages: any;
  currentLang: any = 'en';
  baseImageUrl = environment.baseImageUrl;
  constructor(
    private router: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private blogService: BlogService,
    private languageService: LanguageService
  ) {
    this.languageService.currentLangauge.subscribe((res: any) => {
      if (res == null || res == 'en') {
        this.currentLang = 'en';
      } else {
        this.currentLang = res;
      }
    });
    this.router.params.subscribe((params) => {
      this.spinner.show();
      this.blogService.getBlogDetail(params['id']).subscribe((res: any) => {
        this.blogDetails = res.data;
        this.blogImages = res.data.blogImages;
        this.spinner.hide();
      });
    });
  }

  ngOnInit(): void {}
}
