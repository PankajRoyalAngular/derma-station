import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { Filters } from '../../shared/modals/filter';
import { BlogService } from '../../shared/services';
import { environment } from '../../../environments/environment.prod';
import { Router } from '@angular/router';
@Component({
  selector: 'app-all-blogs',
  templateUrl: './all-blogs.component.html',
  styleUrls: ['./all-blogs.component.css'],
})
export class AllBlogsComponent implements OnInit {
  filters: Filters = {
    sortOrder: '',
    sortField: '',
    pageNumber: 1,
    pageSize: 100,
    productCategoryId: 1,
    searchQuery: '',
    filterBy: '',
  };
  @ViewChild('backtotop')
  backtotop: ElementRef;
  page: number = 1;
  blogsData: any;
  baseImageUrl = environment.baseImageUrl;
  constructor(
    private blogService: BlogService,
    private spinner: NgxSpinnerService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.getAllBlogs();
  }

  getAllBlogs() {
    this.spinner.show();
    this.blogService.getAllBlogs(this.filters).subscribe((res: any) => {
      this.blogsData = res.data.dataList;
      this.spinner.hide();
    });
  }

  gotoAllBlogs() {
    this.page = 1;
    setTimeout(function () {
      var elmnt = document.getElementById('backtotop');
      elmnt.scrollIntoView({
        behavior: 'smooth',
      });
    }, 0);
    this.router.navigateByUrl('/all-blogs');
  }

  getSkinCareBlogs() {
    this.page = 1;
    this.filters.productCategoryId = 1;
    this.spinner.show();
    this.blogService
      .getBlogListByCategory(this.filters)
      .subscribe((res: any) => {
        this.blogsData = res.data.dataList;
        this.spinner.hide();
      });
  }

  getHairCareBlogs() {
    this.page = 1;
    this.filters.productCategoryId = 2;
    this.spinner.show();
    this.blogService
      .getBlogListByCategory(this.filters)
      .subscribe((res: any) => {
        this.blogsData = res.data.dataList;
        this.spinner.hide();
      });
  }

  goTotop() {
    setTimeout(function () {
      var elmnt = document.getElementById('backtotop');
      elmnt.scrollIntoView({
        behavior: 'smooth',
      });
    }, 0);
  }

  readMore(data: any) {
    setTimeout(function () {
      var elmnt = document.getElementById('backtotop');
      elmnt.scrollIntoView({ behavior: 'smooth' });
    }, 100);
    this.router.navigate(['/blog/blog-details/', data.id]);
  }
}
