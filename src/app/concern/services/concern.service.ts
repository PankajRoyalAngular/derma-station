import { Injectable } from '@angular/core';
import { ApiEndPoint } from 'src/app/shared/enums/api-end-point.enum';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class ConcernService {
  constructor(private http: HttpClient) {}

  //Get AllConcerns List
  getAllConcerns() {
    return this.http.get(environment.baseUrl + ApiEndPoint.getAllConcerns);
  }

  getProductDetailsForConcern(id: any) {
    return this.http.get(
      environment.baseUrl +
        ApiEndPoint.getProductDetailsForConcern +
        '?concernId=' +
        id
    );
  }
}
