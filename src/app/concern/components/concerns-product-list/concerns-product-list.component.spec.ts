import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConcernsProductListComponent } from './concerns-product-list.component';

describe('ConcernsProductListComponent', () => {
  let component: ConcernsProductListComponent;
  let fixture: ComponentFixture<ConcernsProductListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConcernsProductListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConcernsProductListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
