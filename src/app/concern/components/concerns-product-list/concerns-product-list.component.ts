import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { MenuService } from '../../../shared/services/menu.service';
import { LanguageService } from '../../../shared/services/language.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ProductService } from 'src/app/shared/services';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-concerns-product-list',
  templateUrl: './concerns-product-list.component.html',
  styleUrls: ['./concerns-product-list.component.css'],
})
export class ConcernsProductListComponent implements OnInit {
  concernData: any;
  rate: number = 5;
  titleValue: any;
  isShownodata: boolean = false;
  isReadonly: boolean = true;
  currentLang: any;
  baseImageUrl = environment.baseImageUrl;
  subscription: Subscription;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private productService: ProductService,
    private spinner: NgxSpinnerService,
    private languageService: LanguageService,
    private toastr: ToastrService
  ) {
    this.languageService.currentLangauge.subscribe((res: any) => {
      if (res == null || res == 'en') {
        this.currentLang = 'en';
      } else {
        this.currentLang = res;
      }
    });
    if (this.router.url == '/concern/concerns-product-list') {
      this.router.navigateByUrl('concern/all-concerns');
    }
  }
  ngOnInit(): void {
    this.spinner.show();
    this.route.params.subscribe((params) => {
      var productdata = {
        pageNumber: 1,
        pageSize: 10,
        inputValue: params['id'],
      };
      // Use for future to send id console.log(params['id']);
      this.productService
        .getProductDetailsForConcern(productdata)
        .subscribe((res: any) => {
          if (res.data.dataList.length == 0) {
            this.isShownodata = true;
            this.spinner.hide();
          } else {
            this.isShownodata = false;
            this.concernData = res.data.dataList;
            this.titleValue = res.data.dataList;
            this.rate = res.data.dataList.rating;
            this.spinner.hide();
          }
        });
    });
  }

  productDetails(data: any) {
    setTimeout(function () {
      var elmnt = document.getElementById('backtotop');
      elmnt.scrollIntoView({ behavior: 'smooth' });
    }, 0);
    this.router.navigate(['/concern/concerns-details/', data.productId]);
  }

  ngAfterViewInit() {
    window.scrollTo(0, 0);
  }
}
