import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import { LanguageService, MenuService } from 'src/app/shared/services';
import { ConcernService } from '../../services/concern.service';

@Component({
  selector: 'app-all-concerns',
  templateUrl: './all-concerns.component.html',
  styleUrls: ['./all-concerns.component.css'],
})
export class AllConcernsComponent implements OnInit {
  allConcernsList: any;
  currentLang: any;
  constructor(
    private concernService: ConcernService,
    private spinner: NgxSpinnerService,
    private languageService: LanguageService,
    private router: Router,
    private menuService: MenuService
  ) {
    this.languageService.currentLangauge.subscribe((res: any) => {
      if (res == null || res == 'en') {
        this.currentLang = 'en';
      } else {
        this.currentLang = res;
      }
    });
  }

  ngOnInit() {
    this.getAllConcerns();
  }

  getAllConcerns() {
    this.spinner.show();
    this.concernService.getAllConcerns().subscribe((res: any) => {
      const sorted = res.data.sort((a, b) =>
        a.concernName > b.concernName ? 1 : -1
      );
      const grouped = sorted.reduce((groups, contact) => {
        const letter = contact.concernName.charAt(0);
        groups[letter] = groups[letter] || [];
        groups[letter].push(contact);
        return groups;
      }, {});

      const result = Object.keys(grouped).map((key) => ({
        key,
        contacts: grouped[key],
      }));
      this.allConcernsList = result;
      this.spinner.hide();
    });
  }
  gotoDiv(content: any) {
    document.getElementById(content).scrollIntoView({ behavior: 'smooth' });
  }

  getconcernsDetails(data: any) {
    this.menuService.sharedData(data);
    localStorage.setItem('sourceData', JSON.stringify(data));
    this.router.navigate(['concern/concerns-product-list', data.concernId]);
  }
}
