import { Component, OnInit, TemplateRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { ProductDetailService, MenuService } from 'src/app/shared/services';
import { AddToBagService } from 'src/app/shared/services/add-to-bag.service';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-concerns-product-detail',
  templateUrl: './concerns-product-detail.component.html',
  styleUrls: ['./concerns-product-detail.component.css'],
})
export class ConcernsProductDetailComponent implements OnInit {
  concernsProductDetails: any;
  cartValue: any = 0;
  baseImageUrl = environment.baseImageUrl;
  myThumbnail: string;
  modalRef: BsModalRef;
  constructor(
    private route: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private productDetailService: ProductDetailService,
    private menuService: MenuService,
    private modalService: BsModalService,
    private addtocartService: AddToBagService
  ) {
    this.route.params.subscribe((res: any) => {
      {
        this.spinner.show();
        this.productDetailService
          .getProductInfoById(res['id'])
          .subscribe((res: any) => {
            if (res.status) {
              this.spinner.hide();
              this.concernsProductDetails = res.data;
              this.myThumbnail =
                this.baseImageUrl + this.concernsProductDetails.imagePaths[0];
            } else {
              this.spinner.hide();
              this.menuService.sharedData(null);
            }
          });
      }
    });
  }
  ngOnInit() {}

  openAddToCart(template: TemplateRef<any>, productData: any) {
    this.modalRef = this.modalService.show(template);
    var data = {
      cartId: localStorage.getItem('cartId'),
      quantity: 1,
      price: productData.attributes.price,
      productId: productData.attributes.productId,
    };
    this.addtocartService.addToCart(data).subscribe((res: any) => {
      console.log(res);
      this.addtocartService.cardItemCount(res.totalCartItems);
    });
  }
}
