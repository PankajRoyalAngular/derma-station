import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConcernsProductDetailComponent } from './concerns-product-detail.component';

describe('ConcernsProductDetailComponent', () => {
  let component: ConcernsProductDetailComponent;
  let fixture: ComponentFixture<ConcernsProductDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConcernsProductDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConcernsProductDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
