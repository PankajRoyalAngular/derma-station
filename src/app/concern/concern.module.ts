import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConcernRoutingModule } from './concern-routing.module';
import { SharedModule } from '../shared/shared.module';
import { ConcernComponent } from './concern.component';
import {
  AllConcernsComponent,
  ConcernsProductDetailComponent,
  ConcernsProductListComponent,
} from '.';

@NgModule({
  declarations: [
    AllConcernsComponent,
    ConcernsProductListComponent,
    ConcernComponent,
    ConcernsProductDetailComponent,
  ],
  imports: [CommonModule, ConcernRoutingModule, SharedModule],
})
export class ConcernModule {}
