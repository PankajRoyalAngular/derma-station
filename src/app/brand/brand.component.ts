import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-brand',
  templateUrl: './brand.component.html',
  styleUrls: ['./brand.component.css'],
})
export class BrandComponent implements OnInit {
  constructor(private router: Router) {
    if (this.router.url === '/brand' || '/brand/brands-product-list') {
      this.router.navigateByUrl('brand/all-brands');
    }
  }

  ngOnInit(): void {}
}
