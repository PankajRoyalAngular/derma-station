import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, RouterModule } from '@angular/router';
import { LanguageService } from '../../../shared/services/language.service';
import { Subscription } from 'rxjs';
import { ProductService } from '../../../shared/services/product.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-brand-product-list',
  templateUrl: './brand-product-list.component.html',
  styleUrls: ['./brand-product-list.component.css'],
})
export class BrandProductListComponent implements OnInit {
  brandData: any;
  rate: number = 5;
  isShownodata: boolean = false;
  titleValue: any;
  isShowBrandData: boolean = false;
  isReadonly: boolean = true;
  currentLang: any;
  baseImageUrl = environment.baseImageUrl;
  subscription: Subscription;
  routeSub: any;
  constructor(
    private router: Router,
    private productService: ProductService,
    private spinner: NgxSpinnerService,
    private languageService: LanguageService,
    private route: ActivatedRoute
  ) {
    this.languageService.currentLangauge.subscribe((res: any) => {
      if (res == null || res == 'en') {
        this.currentLang = 'en';
      } else {
        this.currentLang = res;
      }
    });
    if (this.router.url == '/brand/brands-product-list') {
      this.router.navigateByUrl('brand/all-brands');
    }
  }

  ngOnInit(): void {
    this.spinner.show();
    this.route.params.subscribe((params) => {
      var productData = {
        pageNumber: 1,
        pageSize: 10,
        inputValue: atob(params['id']),
      };
      // Use for future to send id console.log(params['id']);
      this.productService
        .getProductDetailsForBrand(productData)
        .subscribe((res: any) => {
          if (res.data.dataList.length === 0) {
            this.isShownodata = true;
            this.spinner.hide();
          } else {
            this.isShownodata = false;
            this.brandData = res.data.dataList;
            this.titleValue = res.data.dataList;
            this.rate = res.data.dataList.rating;
            this.spinner.hide();
          }
        });
    });
  }

  productDetails(data: any) {
    setTimeout(function () {
      var elmnt = document.getElementById('backtotop');
      elmnt.scrollIntoView({ behavior: 'smooth' });
    }, 0);
    this.router.navigate(['/brand/brands-detail/' + btoa(data.productId)]);
  }

  ngAfterViewInit() {
    window.scrollTo(0, 0);
  }
}
