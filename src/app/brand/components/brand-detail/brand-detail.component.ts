import { Component, OnInit, TemplateRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { MenuService } from '../../../shared/services/menu.service';
import { ProductDetailService } from '../../../shared/services/product-detail.service';
import { environment } from 'src/environments/environment.prod';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { AddToBagService } from '../../../shared/services/add-to-bag.service';

@Component({
  selector: 'app-brand-detail',
  templateUrl: './brand-detail.component.html',
  styleUrls: ['./brand-detail.component.css'],
})
export class BrandDetailComponent implements OnInit {
  quantityValue: Array<Object> = [
    { value: 1 },
    { value: 2 },
    { value: 3 },
    { value: 4 },
    { value: 5 },
  ];
  quantityValueForcart: any = 1;
  brandProductDetails: any;
  cartValue: any = 0;
  baseImageUrl = environment.baseImageUrl;
  myThumbnail: string;
  modalRef: BsModalRef;
  constructor(
    private route: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private productDetailService: ProductDetailService,
    private menuService: MenuService,
    private modalService: BsModalService,
    private addtocartService: AddToBagService
  ) {
    this.route.params.subscribe((res: any) => {
      {
        this.spinner.show();
        this.productDetailService
          .getProductInfoById(atob(res['id']))
          .subscribe((res: any) => {
            if (res.status) {
              this.spinner.hide();
              this.brandProductDetails = res.data;
              this.myThumbnail =
                this.baseImageUrl + this.brandProductDetails.imagePaths[0];
            } else {
              this.spinner.hide();
              this.menuService.sharedData(null);
            }
          });
      }
    });
  }

  ngOnInit(): void {}

  openAddToCart(template: TemplateRef<any>, productData: any) {
    console.log(productData.attributes);
    this.modalRef = this.modalService.show(template);
    var data = {
      cartId: 0,
      quantity: this.quantityValueForcart,
      price: productData.attributes.price,
      productId: productData.attributes.productId,
    };
    this.addtocartService.addToCart(data).subscribe((res: any) => {
      localStorage.setItem('cartId', res.cartId);
      this.addtocartService.cardItemCount(res.totalCartItems);
    });
  }

  onSelectQuantity(value: any) {
    this.quantityValueForcart = parseInt(value);
  }
}
