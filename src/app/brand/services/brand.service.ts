import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiEndPoint } from 'src/app/shared/enums/api-end-point.enum';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class BrandService {
  constructor(private http: HttpClient) {}

  //Get All Brand List
  getAllBrands() {
    return this.http.get(environment.baseUrl + ApiEndPoint.getAllBrands);
  }

  //Get Product List Of Particular Brand
  getProductDetailsForBrand(id: any) {
    return this.http.get(
      environment.baseUrl +
        ApiEndPoint.getProductDetailsForBrand +
        '?brandId=' +
        id
    );
  }
}
