import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BrandRoutingModule } from './brand-routing.module';
import { SharedModule } from '../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  AllBrandsComponent,
  BrandComponent,
  BrandDetailComponent,
  BrandProductListComponent,
} from '.';
import { RatingModule } from 'ngx-bootstrap/rating';
import { ModalModule } from 'ngx-bootstrap/modal';

@NgModule({
  declarations: [
    BrandComponent,
    AllBrandsComponent,
    BrandProductListComponent,
    BrandDetailComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ModalModule,
    BrandRoutingModule,
    SharedModule,
    RatingModule,
  ],
})
export class BrandModule {}
