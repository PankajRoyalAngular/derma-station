import { Component, OnInit } from '@angular/core';
import { LanguageService } from '../shared/services';

@Component({
  selector: 'app-consult-doctor',
  templateUrl: './consult-doctor.component.html',
  styleUrls: ['./consult-doctor.component.css'],
})
export class ConsultDoctorComponent implements OnInit {
  currentLang: any;
  constructor(private languageService: LanguageService) {
    this.languageService.currentLangauge.subscribe((res: any) => {
      if (res == null || res == 'en') {
        this.currentLang = 'en';
      } else {
        this.currentLang = res;
      }
    });
  }

  ngOnInit(): void {}
}
