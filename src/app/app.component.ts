import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { LanguageService } from './shared/services/language.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'derma-station';
  lang: any;
  constructor(
    public translate: TranslateService,
    private spinner: NgxSpinnerService,
    private languageService: LanguageService
  ) {
    var lang: any = localStorage.getItem('lang');
    if (lang == 'ar') {
      this.languageService.shareLanguage(lang);
      this.refreshLanguage('ar');
    } else {
      this.languageService.shareLanguage(lang);
      this.refreshLanguage('en');
    }
  }

  ngOnInit(): void {
    setTimeout(() => {
      this.spinner.hide();
    }, 30000);
  }

  refreshLanguage(language: any) {
    this.translate.setDefaultLang(language);
    this.translate.use(language);
    if (language == 'ar') {
      this.lang = 'en';
      document.body.setAttribute('dir', 'rtl');
      document.body.classList.add('arabic-page');
    } else {
      this.lang = 'ar';
      document.body.setAttribute('dir', 'ltr');
      document.body.classList.remove('arabic-page');
    }
  }

  onDeactivate() {
    document.body.scrollTop = 0;
  }
}
