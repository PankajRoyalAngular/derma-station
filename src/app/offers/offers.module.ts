import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OffersRoutingModule } from './offers-routing.module';
import { OffersComponent } from './offers.component';
import { SharedModule } from '../shared/shared.module';
import { FeaturedOffersComponent } from './featured-offers/featured-offers.component';
import { EverydayOffersComponent } from './everyday-offers/everyday-offers.component';

@NgModule({
  declarations: [
    OffersComponent,
    FeaturedOffersComponent,
    EverydayOffersComponent,
  ],
  imports: [CommonModule, SharedModule, OffersRoutingModule],
})
export class OffersModule {}
