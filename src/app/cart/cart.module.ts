import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CartRoutingModule } from './cart-routing.module';

import { CartComponent, CheckoutComponent } from '.';

@NgModule({
  declarations: [CheckoutComponent, CartComponent],
  imports: [CommonModule, CartRoutingModule],
})
export class CartModule {}
