import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SkintypeProductListComponent } from './skintype-product-list.component';

describe('SkintypeProductListComponent', () => {
  let component: SkintypeProductListComponent;
  let fixture: ComponentFixture<SkintypeProductListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SkintypeProductListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SkintypeProductListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
