import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { LanguageService } from 'src/app/shared/services';
import { NgxSpinnerService } from 'ngx-spinner';
import { ProductService } from '../../shared/services/product.service';
import { ToastrService } from 'ngx-toastr';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-skintype-product-list',
  templateUrl: './skintype-product-list.component.html',
  styleUrls: ['./skintype-product-list.component.css'],
})
export class SkintypeProductListComponent implements OnInit {
  skinTypeData: any;
  skinData: any;
  currentLang: any;
  titleValue: any;
  rate: any;
  isShowData: boolean = false;
  subscription: Subscription;
  isShownodata: boolean = false;
  baseImageUrl = environment.baseImageUrl;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private toaster: ToastrService,
    private languageService: LanguageService,
    private spinner: NgxSpinnerService,
    private productService: ProductService
  ) {
    this.languageService.currentLangauge.subscribe((res: any) => {
      if (res == null || res == 'en') {
        this.currentLang = 'en';
      } else {
        this.currentLang = res;
      }
    });
    if (this.router.url == '/skin/skintype-product-list') {
      this.router.navigateByUrl('skin/all-skintypes');
    }
  }

  ngOnInit(): void {
    this.spinner.show();
    this.route.params.subscribe((params) => {
      var productData = {
        pageNumber: 1,
        pageSize: 10,
        inputValue: params['id'],
      };
      // Use for future to send id console.log(params['id']);
      this.productService
        .getProductDetailsForSkinType(productData)
        .subscribe((res: any) => {
          if (res.data.dataList.length === 0) {
            this.isShownodata = true;
            this.spinner.hide();
          } else {
            this.isShownodata = false;
            this.skinTypeData = res.data.dataList;
            this.titleValue = res.data.dataList;
            this.rate = res.data.dataList.rating;
            this.spinner.hide();
          }
        });
    });
  }

  productDetails(data: any) {
    console.log(data);
    setTimeout(function () {
      var elmnt = document.getElementById('backtotop');
      elmnt.scrollIntoView();
    }, 500);
    this.router.navigate(['/skin/skintype-detail/', data.productId]);
  }

  ngAfterViewInit() {
    window.scrollTo(0, 0);
  }
}
