import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-skin',
  templateUrl: './skin.component.html',
  styleUrls: ['./skin.component.css'],
})
export class SkinComponent implements OnInit {
  constructor(private router: Router) {
    if (this.router.url === '/skin') {
      this.router.navigateByUrl('skin/all-skintypes');
    }
  }

  ngOnInit(): void {}
}
