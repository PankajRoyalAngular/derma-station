import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { LanguageService, MenuService } from 'src/app/shared/services';

@Component({
  selector: 'app-all-skintype',
  templateUrl: './all-skintype.component.html',
  styleUrls: ['./all-skintype.component.css'],
})
export class AllSkintypeComponent implements OnInit {
  allSkinTypeList: any;
  currentLang: any;
  constructor(
    private menuService: MenuService,
    private spinner: NgxSpinnerService,
    private languageService: LanguageService,
    private router: Router
  ) {
    this.languageService.currentLangauge.subscribe((res: any) => {
      if (res == null || res == 'en') {
        this.currentLang = 'en';
      } else {
        this.currentLang = res;
      }
    });
  }

  ngOnInit(): void {
    this.getAllSkinTypes();
  }

  getAllSkinTypes() {
    this.spinner.show();
    this.menuService.getAllSkinTypes().subscribe((res: any) => {
      const sorted = res.data.sort((a, b) =>
        a.skinTypeName > b.skinTypeName ? 1 : -1
      );
      const grouped = sorted.reduce((groups, contact) => {
        const letter = contact.skinTypeName.charAt(0);
        groups[letter] = groups[letter] || [];
        groups[letter].push(contact);
        return groups;
      }, {});

      const result = Object.keys(grouped).map((key) => ({
        key,
        contacts: grouped[key],
      }));
      this.allSkinTypeList = result;
      this.spinner.hide();
    });
  }
  gotoDiv(content: any) {
    document.getElementById(content).scrollIntoView({ behavior: 'smooth' });
  }

  getskinTypeList(data: any) {
    this.menuService.sharedData(data);
    localStorage.setItem('sourceData', JSON.stringify(data));
    this.router.navigate(['skin/skintype-product-list', data.skinTypeId]);
  }
}
