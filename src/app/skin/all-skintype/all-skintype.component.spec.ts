import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllSkintypeComponent } from './all-skintype.component';

describe('AllSkintypeComponent', () => {
  let component: AllSkintypeComponent;
  let fixture: ComponentFixture<AllSkintypeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllSkintypeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AllSkintypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
