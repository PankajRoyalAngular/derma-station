import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AllSkintypeComponent } from './all-skintype/all-skintype.component';
import { SkinComponent } from './skin.component';
import { SkintypeProductListComponent } from './skintype-product-list/skintype-product-list.component';
import { SkintypeDetailComponent } from './skintype-detail/skintype-detail.component';
const routes: Routes = [
  {
    path: '',
    component: SkinComponent,
  },
  {
    path: 'all-skintypes',
    component: AllSkintypeComponent,
  },
  {
    path: 'skintype-product-list',
    component: SkintypeProductListComponent,
  },
  {
    path: 'skintype-product-list/:id',
    component: SkintypeProductListComponent,
  },
  {
    path: 'skintype-detail/:id',
    component: SkintypeDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SkinRoutingModule {}
