import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SkintypeDetailComponent } from './skintype-detail.component';

describe('SkintypeDetailComponent', () => {
  let component: SkintypeDetailComponent;
  let fixture: ComponentFixture<SkintypeDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SkintypeDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SkintypeDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
