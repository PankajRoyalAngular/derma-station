import { Component, OnInit, TemplateRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { ProductDetailService, MenuService } from 'src/app/shared/services';
import { AddToBagService } from 'src/app/shared/services/add-to-bag.service';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-skintype-detail',
  templateUrl: './skintype-detail.component.html',
  styleUrls: ['./skintype-detail.component.css'],
})
export class SkintypeDetailComponent implements OnInit {
  skinTypeProductDetails: any;
  baseImageUrl = environment.baseImageUrl;
  myThumbnail: string;
  modalRef: BsModalRef;
  constructor(
    private route: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private modalService: BsModalService,
    private productService: ProductDetailService,
    private menuService: MenuService,
    private addtocartService: AddToBagService
  ) {
    this.route.params.subscribe((res: any) => {
      {
        this.spinner.show();
        this.productService
          .getProductInfoById(res['id'])
          .subscribe((res: any) => {
            if (res.status) {
              this.skinTypeProductDetails = res.data;
              this.myThumbnail =
                this.baseImageUrl + this.skinTypeProductDetails.imagePaths[0];
              this.spinner.hide();
              this.menuService.sharedData(null);
            } else {
              this.menuService.sharedData(null);
              this.spinner.hide();
            }
          });
      }
    });
  }

  ngOnInit(): void {}

  openAddToCart(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
    var data = {
      cartId: 0,
      quantity: 1,
      price: 25,
      productId: 14,
    };
    this.addtocartService.addToCart(data).subscribe((res: any) => {
      console.log(res);
    });
  }
}
