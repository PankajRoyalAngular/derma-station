import { Component, OnInit } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { NgxSpinnerService } from 'ngx-spinner';
import { environment } from 'src/environments/environment.prod';
import { ProductService } from '../../services';
import { Router } from '@angular/router';

@Component({
  selector: 'app-best-seller',
  templateUrl: './best-seller.component.html',
  styleUrls: ['./best-seller.component.css'],
})
export class BestSellerComponent implements OnInit {
  productListForBestSellers: [];
  baseImageUrl = environment.baseImageUrl;
  customOptions: OwlOptions = {
    loop: false,
    autoplay: true,
    mouseDrag: true,
    touchDrag: false,
    pullDrag: false,

    dots: true,
    navSpeed: 700,
    navText: ['', ''],
    responsive: {
      0: {
        items: 1,
      },
      400: {
        items: 2,
      },
      740: {
        items: 3,
      },
      940: {
        items: 3,
      },
    },
    nav: false,
  };
  constructor(
    private productService: ProductService,
    private spinner: NgxSpinnerService,
    private router: Router
  ) {
    this.spinner.show();
    this.productService.getProductListForBestSellers().subscribe((res: any) => {
      this.productListForBestSellers = res.data;
      this.spinner.hide();
    });
  }

  ngOnInit(): void {}

  productDetails(data: any) {
    setTimeout(function () {
      var elmnt = document.getElementById('backtotop');
      elmnt.scrollIntoView({ behavior: 'smooth' });
    }, 0);
    this.router.navigate(['/brand/brands-detail/' + btoa(data.productId)]);
  }
}
