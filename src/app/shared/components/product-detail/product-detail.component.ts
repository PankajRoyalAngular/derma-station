import { Component, OnInit } from '@angular/core';
import { ConcernService } from 'src/app/concern/services/concern.service';
import { ProductService } from '../../services/product.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Subscription } from 'rxjs';
import { MenuService } from '../../services/menu.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css'],
})
export class ProductDetailComponent implements OnInit {
  sourceData: any;
  rate: number = 5;
  isReadonly: boolean = true;
  brandProductDetails: any;
  subscription: Subscription;

  myThumbnail = './assets/images/product-2.jpg';
  constructor(
    private productService: ProductService,
    private concernService: ConcernService,
    private spinner: NgxSpinnerService,
    private menuService: MenuService
  ) {
    this.subscription = this.menuService.datatoShare.subscribe((res: any) => {
      this.sourceData = res;
      if (this.sourceData.brandId) {
        this.spinner.show();
        this.productService
          .getProductDetailsForBrand('1')
          .subscribe((res: any) => {
            this.brandProductDetails = res.data[0];
            this.rate = res.data[0].rating;
            this.spinner.hide();
            this.menuService.sharedData(null);
          });
      } else if (this.sourceData.concernId) {
        this.spinner.show();
        this.concernService
          .getProductDetailsForConcern(this.sourceData.concernId)
          .subscribe((res: any) => {
            this.spinner.hide();
            this.brandProductDetails = res.data[0];
            this.rate = res.data[0].rating;
            this.spinner.hide();
            this.menuService.sharedData(null);
          });
      } else if (this.sourceData.skinTypeId) {
        this.spinner.show();
        this.productService
          .getProductDetailsForSkinType(this.sourceData.skinTypeId)
          .subscribe((res: any) => {
            this.brandProductDetails = res.data[0];
            this.rate = res.data[0].rating;
            this.spinner.hide();
            this.menuService.sharedData(null);
          });
      } else if (this.sourceData.ingredientId) {
        this.spinner.show();
        this.productService
          .getProductDetailsForIngredient(this.sourceData.ingredientId)
          .subscribe((res: any) => {
            this.brandProductDetails = res.data[0];
            this.rate = res.data[0].rating;
            this.spinner.hide();
            this.menuService.sharedData(null);
          });
      } else {
        this.spinner.hide();
        console.log('error');
      }
    });
  }

  ngOnInit(): void {}

  ngAfterViewInit() {
    window.scrollTo(0, 0);
  }
}
