import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css'],
})
export class FooterComponent implements OnInit {
  constructor(private router: Router, private spinner: NgxSpinnerService) {}
  ngOnInit(): void {}

  gotoPage(routerUrl: any) {
    this.spinner.show();
    setTimeout(function () {
      var elmnt = document.getElementById('backtotop');
      elmnt.scrollIntoView({
        behavior: 'smooth',
      });
    }, 0);
    this.router.navigateByUrl(routerUrl);
    this.spinner.hide();
  }
}
