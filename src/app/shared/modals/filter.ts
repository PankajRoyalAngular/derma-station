export interface Filters {
  sortOrder: string;
  sortField: string;
  pageNumber: number;
  pageSize: number;
  productCategoryId: number;
  searchQuery: string;
  filterBy: string;
}
