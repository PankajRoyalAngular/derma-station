export interface ContactUs {
  infoId: number;
  firstName: string;
  lastName: string;
  email: string;
  firstNameInAr: string;
  lastNameInAr: string;
  emailInAr: string;
  phone: string;
  message: string;
}
