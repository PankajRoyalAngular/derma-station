//Modules
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedRoutingModule } from './shared-routing.module';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { NgxSpinnerModule } from 'ngx-spinner';
import { MaterialModule } from '../material.module';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { HttpClientModule } from '@angular/common/http';
import { TranslateModule } from '@ngx-translate/core';

//Shared Components
import {
  CategoriesComponent,
  BestSellerComponent,
  ClientsComponent,
  ProductComponent,
  KeepInTouchComponent,
  BannerComponent,
  BenefitsComponent,
  TestimonialsComponent,
  OurProductsComponent,
  FilterUiComponent,
  AboutUsComponent,
  ContactUsComponent,
  PoliciesReturnsComponent,
  PrivacyPolicyComponent,
  ProductDetailComponent,
} from './components';

//Layout Componets
import { HeaderComponent, FooterComponent } from './layouts';
import { NgxPaginationModule } from 'ngx-pagination';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OnlyEnglishDirective } from './directives/onlyEnglish';
import { OnlyArbicDirective } from './directives/onlyArabic';
import { ProductFilterComponent } from './components/product-filter/product-filter.component';
import { NgxImageZoomModule } from 'ngx-image-zoom';
import { RatingModule } from 'ngx-bootstrap/rating';
@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    CategoriesComponent,
    BestSellerComponent,
    ClientsComponent,
    ProductComponent,
    KeepInTouchComponent,
    BannerComponent,
    BenefitsComponent,
    TestimonialsComponent,
    OurProductsComponent,
    FilterUiComponent,
    AboutUsComponent,
    ContactUsComponent,
    PrivacyPolicyComponent,
    ProductDetailComponent,
    PoliciesReturnsComponent,
    OnlyEnglishDirective,
    OnlyArbicDirective,
    ProductFilterComponent,
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    SharedRoutingModule,
    CarouselModule,
    TranslateModule,
    MaterialModule,
    CollapseModule,
    NgxSpinnerModule,
    BsDropdownModule,
    NgxPaginationModule,
    AccordionModule,
    NgxImageZoomModule,
    BsDatepickerModule,
    RatingModule,
  ],
  exports: [
    HeaderComponent,
    FooterComponent,
    CategoriesComponent,
    BestSellerComponent,
    ClientsComponent,
    KeepInTouchComponent,
    ProductComponent,
    BannerComponent,
    BenefitsComponent,
    TestimonialsComponent,
    OurProductsComponent,
    ProductFilterComponent,
    CarouselModule,
    NgxImageZoomModule,
    TranslateModule,
    FilterUiComponent,
    OnlyEnglishDirective,
    OnlyArbicDirective,
    NgxPaginationModule,
    RatingModule,
  ],
})
export class SharedModule {}
