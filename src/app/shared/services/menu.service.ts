import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ApiEndPoint } from '../enums/api-end-point.enum';

@Injectable({
  providedIn: 'root',
})
export class MenuService {
  private dataSource = new BehaviorSubject(null);
  datatoShare = this.dataSource.asObservable();
  constructor(private http: HttpClient) {}

  sharedData(data: string) {
    this.dataSource.next(data);
  }

  //Get All Skin Care
  getSkinCare() {
    return this.http.get(environment.baseUrl + ApiEndPoint.getSkinCare);
  }

  //Get Browse By List
  browsebylist() {
    return this.http.get(environment.baseUrl + ApiEndPoint.browsebylist);
  }

  //Get All Ingredients
  getAllIngredients() {
    return this.http.get(environment.baseUrl + ApiEndPoint.getAllIngredients);
  }

  //Get All skinTypes
  getAllSkinTypes() {
    return this.http.get(environment.baseUrl + ApiEndPoint.getAllSkinTypes);
  }
}
