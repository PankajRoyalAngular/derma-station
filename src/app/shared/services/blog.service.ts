import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ApiEndPoint } from '../enums/api-end-point.enum';

@Injectable({
  providedIn: 'root',
})
export class BlogService {
  constructor(private http: HttpClient) {}

  //Get All Blogs
  getAllBlogs(filters: any) {
    return this.http.post(
      environment.baseUrl + ApiEndPoint.getAllBlogs,
      filters
    );
  }

  //Get Blog List By Category
  getBlogListByCategory(filters: any) {
    return this.http.post(
      environment.baseUrl + ApiEndPoint.getAllBlogs,
      filters
    );
  }

  //Get Blog Details
  getBlogDetail(id: any) {
    return this.http.get(
      environment.baseUrl + ApiEndPoint.getBlogDetail + '?blogId=' + id
    );
  }
}
