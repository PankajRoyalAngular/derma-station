import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment.prod';
import { ApiEndPoint } from '../enums/api-end-point.enum';

@Injectable({
  providedIn: 'root',
})
export class ProductDetailService {
  private dataSource = new BehaviorSubject(null);
  datatoShare = this.dataSource.asObservable();
  constructor(private http: HttpClient) {}

  shareData(data: string) {
    this.dataSource.next(data);
  }

  //Get product Info By Id:
  getProductInfoById(id: any) {
    return this.http.get(
      environment.baseUrl + ApiEndPoint.getProductInfoById + '?productId=' + id
    );
  }
}
