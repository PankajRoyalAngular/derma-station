import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment.prod';
import { ApiEndPoint } from '../enums/api-end-point.enum';

@Injectable({
  providedIn: 'root',
})
export class AddToBagService {
  cartitemValue: any;
  constructor(private http: HttpClient) {}
  private cartSource = new BehaviorSubject(null);
  cartValue = this.cartSource.asObservable();
  cardItemCount(data: string) {
    this.cartSource.next(data);
  }

  //Add to Cart
  addToCart(data: any) {
    return this.http.post(environment.baseUrl + ApiEndPoint.addToCart, data);
  }
}
