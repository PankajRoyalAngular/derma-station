import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {
  AboutUsComponent,
  ContactUsComponent,
  FaqComponent,
  PoliciesReturnsComponent,
  PrivacyPolicyComponent,
  ProductDetailComponent,
} from './components';

const routes: Routes = [
  {
    path: 'about-us',
    component: AboutUsComponent,
  },
  {
    path: 'contact-us',
    component: ContactUsComponent,
  },
  {
    path: 'faq',
    component: FaqComponent,
  },
  {
    path: 'privacy-policy',
    component: PrivacyPolicyComponent,
  },
  {
    path: 'product-detail',
    component: ProductDetailComponent,
  },

  {
    path: 'policies',
    component: PoliciesReturnsComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SharedRoutingModule {}
